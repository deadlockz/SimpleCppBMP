#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__ 1

#include <cstdio>
#include <cstdlib>

struct Image {

    typedef unsigned char Byte;

    struct Color {
        Byte blue;
        Byte green;
        Byte red;
    };

    int width;
    int height;

    /**
     * create an empty image data
     */
    void init (int _width, int _height) {
        width  = _width;
        height = _height;
        data = new Color[width * height];
    }
    
    /**
     * set a color
     */
    void set (int x, int y, Color color) {
        get(x,y) = color;
    }
    
    /**
     * get or set a color
     */
    Color & get (int x, int y) {
        return data[pos(x, y)];
    }

    /**
     * copy image object
     */
    void copy (const Image & src) {
        width = src.width;
        height = src.height;
        int size = 3 * (src.width * src.height);
        data = (Color *) (new Byte[size]);
        for (int i=0; i<size; ++i) {
            *(((Byte *) data)+i) = *(((Byte *) src.data)+i);
        }
    }

    /**
     * get or set the blue value
     */
    Byte & B (int x, int y) {
        return *((Byte *) &(data[pos(x, y)]));
    }
    
    /**
     * get or set the green value
     */
    Byte & G (int x, int y) {
        // get address from 3byte Color, cast to "bytes" address, add +1
        // to Address (= 1byte step size) and get value of it
        return *( ((Byte *) &(data[pos(x, y)]))+1 );
    }
    
    /**
     * get or set the red value
     */
    Byte & R (int x, int y) {
        return *( ((Byte *) &(data[pos(x, y)]))+2 );
    }

    void line (int x0, int y0, int x1, int y1, const Color & color) {
        int sx = x0<x1 ? 1 : -1;
        int sy = y0<y1 ? 1 : -1;
        int dx =  sx * (x1 - x0);
        int dy = -1 * sy * (y1 - y0);
        int err = dx+dy, e2; /* error value e_xy */

        while (1) {
            set(x0, y0, color);
            if (x0==x1 && y0==y1) break;
            e2 = 2*err;
            if (e2 > dy) { err += dy; x0 += sx; }
            if (e2 < dx) { err += dx; y0 += sy; }
        }
    }
    
    /**
     * read a bmp file (24bit)
     */
    void load (const char * filename) {
        FILE* f = std::fopen(filename, "rb");
        Byte info[54];
        Byte * row;
        int row_padded;
        
        std::fread(info, sizeof(Byte), 54, f);
        
        width     = *( (int*) &info[18]);
        height    = *( (int*) &info[22]);
        int psize = *( (int*) &info[34]);
        
        data = new Color[width*height];
        
        row_padded = (psize/height);
        row = new Byte[row_padded];
        
        /** sometimes the fileheader is bigger?!?! */
        //fread(row, sizeof(Byte), 68, f);
        
        for(int y = height-1; y >= 0; --y) {
            std::fread(row, sizeof(Byte), row_padded, f);
            for(int x = 0; x < width; ++x) {
                B(x, y) = row[(3*x)];
                G(x, y) = row[(3*x)+1];
                R(x, y) = row[(3*x)+2];
            }
        }
        std::fclose(f);
    }

    /**
     * write a bmp file (24bit)
     */
    void save (const char * filename) {
        FILE* f = std::fopen(filename, "wb");
        Byte info[54] = {
            'B','M',  0,0,0,0, 0,0, 0,0, 54,0,0,0,
            40,0,0,0, 0,0,0,0, 0,0,0,0,  1,0, 24,0,
            0,0,0,0,  0,0,0,0
        };
        int xbytes = 4 - ((width * 3) % 4);
        if (xbytes == 4) xbytes = 0;
        int psize = ((width * 3) + xbytes) * height;
        
        *( (int*) &info[2]) = 54 + psize;
        *( (int*) &info[18]) = width;
        *( (int*) &info[22]) = height;
        *( (int*) &info[34]) = psize;
        
        std::fwrite(info, sizeof(Byte), sizeof(info), f);
        
        int x,y,n;
        for (y=height-1; y>=0; --y) {
            for (x=0; x<width; ++x) {
                std::fprintf(f, "%c", B(x, y));
                std::fprintf(f, "%c", G(x, y));
                std::fprintf(f, "%c", R(x, y));
            }
            // BMP lines must be of lengths divisible by 4
            if (xbytes) {
                for (n = 0; n < xbytes; ++n) std::fprintf(f, "%c", 0);
            }
        }
        
        std::fclose(f);
    }

private:

    int pos (int x, int y) {
        if (x<0) x=0;
        if (x>=width) x=width-1;
        if (y<0) y=0;
        if (y>=height) y=height-1;
        return ((height-y-1)*width +x);
    }
    
    Color * data;
};

#endif
