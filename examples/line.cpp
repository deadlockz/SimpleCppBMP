#include "../Image.hpp"

// g++ -Wall line.cpp -o line.exe

int main(int argc, char * argv[]) {
    Image img;
    Image::Color col;
    col.red   = 255;
    col.green = 0;
    col.blue  = 180;
    
    img.init(300,200);
    img.set(100, 180, col);
    img.line(5, 5, 120, 25, col);
    img.line(105, 5, 10, 25, col);
    col.green = 255;
    img.line(270, 15, 263, 200, col);

    img.save("bmp/lineWithImage.bmp");

    return 0;
}
