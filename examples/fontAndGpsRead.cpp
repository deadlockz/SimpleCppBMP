#include <cstdio> // printf
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>
#include "../Image.hpp"
#include "../Font.hpp"

//g++ fontAndGpsRead.cpp -O2 -mtune=native -Wall -std=c++17 -o fontAndGpsRead.exe

/**
 * reads text files in a folder.
 * every file is displayed as track (different color).
 * each line in a file: lat,lon,distance is a point in the track.
 * distance is a option and not displayed.
 * Filenames with color are in Image as 8x8pixel font.
 */

// europa: lon  -10..25
//         lat   35..70
//#define LON_BOUND -10.0
//#define LON_BOUND_DIF 35.0
//#define LAT_BOUND 35.0
//#define LAT_BOUND_DIF 35.0

// nrw:    lon   5.85..9.47   like x (0=London, 90=Bhutan)
//         lat  50.32..52.52  like y (0=Equartor, 90=Nothpole)
//#define LON_BOUND      5.85
//#define LON_BOUND_DIF  3.62  // 0.01 = 5pixel
//#define LAT_BOUND     50.32
//#define LAT_BOUND_DIF  2.20  // 0.01 = 5pixel
//#define IMAGE_WIDTH  (362*5)
//#define IMAGE_HEIGHT (220*5)

#define LON_BOUND      5.85
#define LON_BOUND_DIF  2.22  // 0.01 = 3pixel
#define LAT_BOUND     50.52
#define LAT_BOUND_DIF  1.42  // 0.01 = 3pixel

#define IMAGE_WIDTH  (222*3)
#define IMAGE_HEIGHT (142*3)

using std::filesystem::directory_iterator;

int MAXCOLOR = 0;

Image img;
Image::Color col;

Image::Color toColor(double id) {
    Image::Color col;
    col.red = 0; col.green = 0; col.blue = 0;
    if( id >= MAXCOLOR || id < 0 ) return col;
    double hi,q,t,coef;

    coef = 5.0 * (id/MAXCOLOR);
    // a kind of floor()
    long long lolo = (long long)coef;
    hi = (double)lolo; // hi => is a selector, which color fade has to be used
    
    t = coef - hi; // id grows up => t=0..1
    q = 1 - t;     // id grows up => q=1..0

    if (hi == 0.0) {
        col.red   = 0;
        col.green = t*200 + 55; // green grows up -> blue to cyan
        col.blue  = 255;
    } else if (hi == 1.0) {
        col.red   = t*255; // red grows up -> cyan to white
        col.green = 255;
        col.blue  = 255;
    } else if (hi == 2.0) {
        col.red   = 255;
        col.green = 255;
        col.blue  = q*255; // blue shrinks down -> white to yellow
    } else if (hi == 3.0) {
        col.red   = 255;
        col.green = q*255; // green shrinks down -> yellow to red
        col.blue  = 0;
    } else if (hi == 4.0) {
        col.red   = 255;
        col.green = 0;
        col.blue  = t*255; // blue grows up -> red to mangenta
    }
    return col;
}

int main(int argc, char * argv[]) {
    img.init(IMAGE_WIDTH, IMAGE_HEIGHT);
    int x0 = 0;
    int y0 = 0;
    double lon = 0.0;
    double lat = 0.0;
    double dist = 0.0;
    char comma;
    
    std::string line;
    int fileno = 0;

    if (argc == 1) {
        printf("Usage: %s [folderpath]\n", argv[0]);
        return 1;
    }
    
    std::string path = argv[1];
    
    for (const auto & file : directory_iterator(path)) {
        if (file.is_regular_file()) MAXCOLOR++;
    }

    for (const auto & file : directory_iterator(path)) {
        
        std::ifstream infile(file.path(), std::ios::in);
        int row = 0;
        
        if (file.is_regular_file() == false) continue;
        
        if (!infile.is_open()) continue;
        
        while (std::getline(infile, line)) {
            std::istringstream iss(line);
            iss >> lat;
            iss >> comma;
            iss >> lon;
            iss >> comma;
            iss >> dist;
            
            double lon1 = lon - LON_BOUND;
            double lat1 = lat - LAT_BOUND;
            
            if (lon1 > LON_BOUND_DIF || lon1 < 0.0) {
                ++row;
                continue;
            }
            
            if (lat1 > LAT_BOUND_DIF || lat1 < 0.0) {
                ++row;
                continue;
            }

            int x1 = IMAGE_WIDTH*(lon1/LON_BOUND_DIF);
            int y1 = IMAGE_HEIGHT - IMAGE_HEIGHT*(lat1/LAT_BOUND_DIF);

            if (row == 0) {
                x0 = x1;
                y0 = y1;
            }
            
            //img.line(x0, y0, x1, y1, toColor(dist)); // needs max distance!
            img.line(x0, y0, x1, y1, toColor(fileno));
        
            x0 = x1;
            y0 = y1;
            ++row;
        }
        Font::print(
            file.path().c_str(),
            file.path().string().length(),
            10, 10+16*fileno,
            toColor(fileno),
            &img
        );
        std::printf("%3d %s processed\n", fileno+1, file.path().c_str());
        infile.close();
        ++fileno;
    }
    
    img.save("bmp/routes.bmp");
    return 0;
}
