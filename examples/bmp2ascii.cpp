#include <cstdio>
#include <cstdlib>
#include "../Image.hpp"

// g++ -Wall bmp2ascii.cpp -o bmp2ascii.exe

// Linux color Escape sequences
#define _NRM  "\x1B[0m"
#define _RED  "\x1B[31m"
#define _GRN  "\x1B[32m"
#define _YEL  "\x1B[33m"
#define _BLU  "\x1B[34m"
#define _PIK  "\x1B[35m"
#define _CYN  "\x1B[36m"
#define _WHT  "\x1B[37m"

using namespace std;

int main(int argc, char * argv[]) {
	Image b;
	
	if (argc > 1) {
		b.load(argv[1]);
	} else {
		printf("Usage: %s [filename]\n", argv[0]);
		return 1;
	}
	
	for (int y=0; y < b.height; ++y) {
		printf("%s", _RED);
		for (int x=0; x < b.width; ++x) {
			int dat = b.get(x, y).red;
			printf("%4d", dat);
		}
		printf("\n%s", _GRN);
		for (int x=0; x < b.width; ++x) {
			int dat = b.get(x, y).green;
			printf("%4d", dat);
		}
		printf("\n%s", _BLU);
		for (int x=0; x < b.width; ++x) {
			int dat = b.get(x, y).blue;
			printf("%4d", dat);
		}
		printf("\n");
	}
	printf("%s", _NRM);
	
	return 0;
}
