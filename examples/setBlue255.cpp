#include <cstdio>
#include <cstdlib>
#include "../Image.hpp"

// g++ -Wall setBlue255.cpp -o setBlue255.exe

using namespace std;

int main(int argc, char * argv[]) {
	Image b;
	
	if (argc > 2) {
		b.load(argv[1]);
	} else {
		printf("Usage: %s [in filename] [out filename]\n", argv[0]);
		return 1;
	}
	
	for (int y=0; y < b.height; ++y) {
		for (int x=0; x < b.width; ++x) {
			// this is possible, because return is a & reference and not a copy
			b.B(x, y) = 255;
		}
	}
	b.save(argv[2]);
	
	return 0;
}
