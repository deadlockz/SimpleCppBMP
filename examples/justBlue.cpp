#include <cstdio>
#include <cstdlib>
#include "../Image.hpp"

// g++ -Wall justBlue.cpp -o justBlue.exe

using namespace std;

int main(int argc, char * argv[]) {
	Image b;
	
	if (argc > 1) {
		b.load(argv[1]);
	} else {
		printf("Usage: %s [filename]\n", argv[0]);
		return 1;
	}
	
	for (int y=0; y < b.height; ++y) {
		for (int x=0; x < b.width; ++x) {
			printf("%4d", (int) b.B(x, y));
		}
		printf("\n");
	}
	
	return 0;
}
