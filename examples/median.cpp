#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include "../Image.hpp"

// g++ -Wall median.cpp -o median.exe

// it is a bit buggy :-(

using namespace std;

template<class T>
T med(vector<T> vals) {
	T val;
	unsigned int size = vals.size();
	sort(vals.begin(), vals.end());
	val = vals[size / 2];
	return val;
}

int main(int argc, char * argv[]) {
	Image b,c;
	
	if (argc > 1) {
		b.load(argv[1]);
	} else {
		printf("Usage: %s [in filename]\n", argv[0]);
		return 1;
	}
	
	c.copy(b);
	
	for (int y=0; y < b.height; ++y) {
		for (int x=0; x < b.width; ++x) {
			vector<Image::Byte> neighborsR;
			vector<Image::Byte> neighborsG;
			vector<Image::Byte> neighborsB;
			
			neighborsR.push_back(b.R(x-1, y-1));
			neighborsR.push_back(b.R(x  , y-1));
			neighborsR.push_back(b.R(x+1, y-1));
			neighborsR.push_back(b.R(x-1, y));
			neighborsR.push_back(b.R(x  , y));
			neighborsR.push_back(b.R(x+1, y));
			neighborsR.push_back(b.R(x-1, y+1));
			neighborsR.push_back(b.R(x  , y+1));
			neighborsR.push_back(b.R(x+1, y+1));
			c.R(x, y) = med<Image::Byte>(neighborsR);
			neighborsG.push_back(b.G(x-1, y-1));
			neighborsG.push_back(b.G(x  , y-1));
			neighborsG.push_back(b.G(x+1, y-1));
			neighborsG.push_back(b.G(x-1, y));
			neighborsG.push_back(b.G(x  , y));
			neighborsG.push_back(b.G(x+1, y));
			neighborsG.push_back(b.G(x-1, y+1));
			neighborsG.push_back(b.G(x  , y+1));
			neighborsG.push_back(b.G(x+1, y+1));
			c.G(x, y) = med<Image::Byte>(neighborsG);
			neighborsB.push_back(b.B(x-1, y-1));
			neighborsB.push_back(b.B(x  , y-1));
			neighborsB.push_back(b.B(x+1, y-1));
			neighborsB.push_back(b.B(x-1, y));
			neighborsB.push_back(b.B(x  , y));
			neighborsB.push_back(b.B(x+1, y));
			neighborsB.push_back(b.B(x-1, y+1));
			neighborsB.push_back(b.B(x  , y+1));
			neighborsB.push_back(b.B(x+1, y+1));
			c.B(x, y) = med<Image::Byte>(neighborsB);
		}
	}
	
	c.save("bmp/mod.bmp");
	return 0;
}
