# Image.hpp

A small class for read and write BMP (Bitmap RGB) files without any 
additional code/framework/stuff. It does not show the image in 
a window - it es just for proccessing the Bitmap data.

A median filter is added :-D, but a bit buggy.

- set pixel and draw lines
- load, modify and save .bmp files
- example to read and draw gps tracks (with `Font.hpp` - a small 8x8 font)

![example GPS Tracks and font](example.jpg)
